---
title: "Panqueca"
date: 2021-09-18T23:28:40-03:00
draft: false
image: /images/panqueca2.jpg
---
&#8231; 1 xícara (chá) de farinha de trigo

&#8231; 1 xícara (chá) de leite

&#8231; 2 colheres (sopa) de manteiga derretida

&#8231; 2 colheres (sopa) de açúcar refinado

&#8231; 1 colher (sopa) de fermento em pó

&#8231; 1 ovo

&#8231; 1 colher (chá) de sal
||||||||||
1º Bata os ovos levemente, junte o leite, a manteiga e o açúcar.

2º Adicione a farinha de trigo, o fermento (caso a farinha não contenha) e o sal. Note que vai ficar um pouco espessa, o que é normal para este tipo de panqueca.

3º Modele as panquecas em uma frigideira antiaderente untada com manteiga.

4º Sirva com mel, geleia, frutas ou iogurte.