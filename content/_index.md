TRADIÇÃO E QUALIDADE 
||||||||||
NOS MÍNIMOS DETALHES
||||||||||
O ROLÊ GOURMET nasceu muito antes de ser uma página da web. Tudo começou na década de 90 com a união de duas famílias, amigas e vizinhas, no tradicional bairro do Ipiranga. Ambas compartilhavam a mesma paixão pelo cozinha, contando moedas da mesada para visitar o lendária restaurante italiano do bairro.
||||||||||
Durante anos, continuaram a se reunir em torno da culinária, criando e aprimorando receitas em busca da perfeição. Mesmo depois de seguirem caminhos profissionais diferentes, os amigos – agora em companhia com seus filhos, também aficionados pelo tema, juntaram-se novamente para resgatar receitas clássicas que fizeram parte da sua infância e colocá-las online. Este é o Rolê Gourmet, que traduz a história das nossas famílias com a tradição da culinária.
||||||||||
OS PRATOS QUE VOCÊ AMA
