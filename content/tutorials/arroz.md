---
title: "Arroz"
date: 2021-09-18T23:28:40-03:00
draft: false
image: /images/arroz2.jpg
---	
&#8231; 1 xícara de arroz lavado

&#8231; 2 xícaras de água fervente

&#8231; 1 dente de alho amassado

&#8231; 1/4 de cebola picada

&#8231; azeite o suficiente

&#8231; sal a gosto
||||||||||
1º Refogue o alho e a cebola no azeite.

2º Coloque o arroz e deixe fritar por cerca de 30 segundos.

3º Adicione a água fervente e o sal.

4º Abaixe o fogo e deixe cozinhar até a água quase secar.

5º Tampe a panela e aguarde cerca de 20 minutos antes de servir.

6º Se desejar fazer mais, é só seguir as proporções, principalmente da água.