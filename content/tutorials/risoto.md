---
title: "Risoto"
date: 2021-09-18T23:28:40-03:00
draft: false
image: /images/risoto2.jpg
---
&#8231; 2 xícaras de arroz cru ou aquele que sobrou na geladeira

&#8231; 2 caldos knorr de carne

&#8231; 500 g de cabotiá em cubos

&#8231; Cheiro verde picado

&#8231; 3 dentes de alho

&#8231; 1 cebola grande picada

&#8231; 1 1/2 litros de água

&#8231; 4 colheres de azeite

&#8231; Sal e pimenta-do-reino a gosto

&#8231; 100 g de mussarela ou queijo parmesão
||||||||||
1º Leve a abóbora ao fogo com a água, o caldo de carne, azeite, alho e a cebola, não precisa refogar.

2º Deixe cozinhar até que a abóbora esteja cozida.

3º Retire do fogo com uma escumadeira, coloque toda a abóbora no liquidificador, retire só um pouco do caldo para bater, reserve o restante do caldo.

4º Depois de batido devolva essa massa ao restante do caldo, acrescente o arroz, o cheiro verde, a pimenta-do-reino e sal a gosto, deixe o arroz cozinhar junto com o caldo.

5º Se for feito com arroz cru, terá que adicionar água para que o risoto não seque.

6º Quando estiver cozido e estiver nem mole demais e nem seco demais, desligue o fogo, acrescente o cheiro verde picado, mexa e leve a uma travessa.

7º Coloque uma camada de risoto, uma de queijo, risoto e por último queijo e bom apetite! Esse risoto é maravilhoso!
